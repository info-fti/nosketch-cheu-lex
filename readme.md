# CHEU-Lex et NoSketchEngine

## Contexte

CHEU-Lex est un projet, mené par la professeure Annarita Felici, est un corpus comparatif fr/it/all de textes législatifs suisses et européens : Voir la page du projet https://transius.unige.ch/en/research/cheu-lex

Ce corpus est mis à disposition par le service informatique de la FTI sur l'url https://public@apps.fti.unige.ch/cheulex/crystal via le logiciel open-source [NosketchEngine](https://nlp.fi.muni.cz/trac/noske).

L'objectif de ce repository est de servir de source de vérité et de stockage pour le corpus et ses éventuelles améliorations, ainsi que de documentation **technique** pour le déploiement et les mises à jour.

## Journal

* 2022.10 : 
   * Demande de mise à jour vers les versions plus récentes. 
   * L'installation locale échouant, création d'un projet gitlab et mise en container (Docker) et résolution d'un bug sed à la compilation du corpus. 
   * Tout est désormais inclu dans apps@ftilnx3:~/nosketch-cheulex
   * L'URL d'accès de l'application est dorénavant : https://public@apps.fti.unige.ch/cheulex/crystal
   * Les composants sont bonito-open-5.57.6/crystal-open-2.124/ manatee-open-2.208/gdex-4.12
* 2020.11 : Instalation initale sur FTILNX3 (bonito-open-4.24.6/crystal-open-2.14/ manatee-open-2.167.10/gdex-3.12) dans divers répertoires du serveur (/var/lib/manatee, /usr/local/lib, /usr/local/bin -> bref le chantier). L'application était disponible via http://corpora.fti.unige.ch/crystal/
 

## Déploiement local

Permet d'utiliser l'application en local, pour par exemple tester un nouveau corpus ou de nouvelles versions des composants.

Le prérequis est d'être membre du projet gitlab.

```bash
cd ~/projects
git clone git@gitlab.unige.ch:info-fti/nosketch-cheu-lex.git
cd nosketch-cheu-lex
cp .env.sample .env  #Cela utilisera le port 8081. S'il est déjà occupé, éditer ce fichier
cp conf/apache-site.conf.sample.dev_local conf/apache-site.conf
docker-compose up -d
docker-compose exec cheulex_nosketch compile_cheulex.sh
xdg-open http://public@127.0.0.1:8081/crystal/
```

A noter : Si l'install docker locale n'est pas rootless, l'étape de compilation va générer des fichiers (p.ex /corpora/cheulex_de/indexed/*) propriété de root.

## Déploiement en production

* Nous utilisons le user 'apps' sur ftilnx3
* Pour ce user, un rootless docker est configuré et sera utilisé.
* L'application est donc totalement confinée (merci Docker) dans apps@ftilnx3:~/nosketch-cheu-lex
* Le déploiement initial à simplement été 
```bash
git clone git@gitlab.unige.ch:info-fti/nosketch-cheu-lex.git
cd nosketch-cheu-lex
cp .env.sample.prod_fti .env
cp conf/apache-site.conf.sample.prod_fti conf/apache-site.conf
docker-compose up -d
docker-compose exec cheulex_nosketch compile_cheulex.sh
```
* L'application écoute sur le port 5003, et le front apache de ftilnx3 fait proxy dont la section pertinente est :
```
    ProxyPass "/cheulex" "http://127.0.0.1:5003/cheulex"
    ProxyPassReverse "/cheulex" "http://127.0.0.1:5003/cheulex"
```
* L'application est redémarrée chaque nuit pour supprimer les jobs de recherches (cf crontab -l)


## Démarrage des service en production

L'application est hébergée sous le user 'apps' et utilise un docker rootless, démarré via une user systemd unit qui va lancer les docker-compose up ...

Le fichier `~/nosketch-cheu-lex/docker-compose-yml` : 

```
version: "3.3"

services:

  cheulex_nosketch:
    image: "registry.gitlab.unige.ch/info-fti/nosketch-cheu-lex:latest"
    ports:
      - "${BINDING_PORT}:80"      
    volumes:
     - "${DOCKER_LOCAL_DATA_PATH}:/corpora" 
     - "./conf/apache-site.conf:/etc/apache2/sites-enabled/000-default.conf:ro"
     - "./conf/htpasswd:/var/lib/bonito/htpasswd:ro"

```

Le fichier `~/.config/systemd/user/nosketch-cheu-lex.service` : 

```
[Unit]
Description=nosketch-cheu-lex
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
WorkingDirectory=%h/nosketch-cheu-lex

Environment="DOCKER_HOST=unix:///run/user/%U/docker.sock"

ExecStart=docker-compose -f %h/nosketch-cheu-lex/docker-compose.yml up --remove-orphans -d
ExecStop=docker-compose -f %h/nosketch-cheu-lex/docker-compose.yml stop

RemainAfterExit=yes
TimeoutSec=120

Restart=on-failure
RestartSec=10

[Install]
WantedBy=default.target
```

il faut naturellement activer les services user correspondants : 

```
systemctl --user enable docker
systemctl --user enable nosketch-cheu-lex
```

## Processus de mise à jour : Corpus
* Tester le corpus en local
  * Modifier les fichiers dans /corpora
  * Compiler le corpus : `docker-compose exec cheulex_nosketch compile_cheulex.sh`
  * Valider que tout fonctionne dans l'app web locale http://public@127.0.0.1:8081/crystal/
  * Comiter les modifications + git push
* Déployer les modification en production : 
```bash
ssh apps@ftilnx3
cd nosketch-cheu-lex
git pull --rebase
docker-compose exec cheulex_nosketch compile_cheulex.sh
```

## Processu de mise à jour : Composants
* Tester les nouveaux composants en local
  * Télécharger les nouveaux tarball
  * Adapter le Dockerfile (il y a deux endroits avec des numéros de version des composants)
  * Builder en local `docker build --tag registry.gitlab.unige.ch/info-fti/nosketch-cheu-lex:latest .`
  * Démarrer l'application en local : `docker-compose down && docker-compose up -d`
  * Compiler le corpus : `docker-compose exec cheulex_nosketch compile_cheulex.sh`
  * Valider que tout fonctionne dans l'app web locale http://public@127.0.0.1:8081/crystal/
  * Si ok : commit du Dockerfile + tarballs
  * Aller sur https://gitlab.unige.ch/info-fti/nosketch-cheu-lex/-/pipelines et vérifier que le pipeline a terminé de construire l'image.
* Déployer en production : 
```bash
ssh apps@ftilnx3
cd nosketch-cheu-lex
git pull --rebase # Optionel
docker-compose pull
docker-compose down && docker-compose up -d
docker-compose exec cheulex_nosketch compile_cheulex.sh
```
* Tester en production. En cas de problème, consulter le précédent tag dans https://gitlab.unige.ch/info-fti/nosketch-cheu-lex/container_registry , le mettre à la main dans docker-compose.yml (à la place de 'latest') et redémarrer l'application, compiler le corpus, etc.


## Création de l'image docker

* Il s'agit d'une utilisation classique du CI gitlab : cf [.gitlab-ci.yml](.gitlab-ci.yml)
* Chaque fois que Dockerfile ou un tarball d'un composasnt est modifié, l'image docker est construite par le CI.
* L'image est `registry.gitlab.unige.ch/info-fti/nosketch-cheu-lex`

## Notes technique sur NoSketch Engine
* Il s'agit de la version open source de 'SketchEngine'
* La version open-source est composée de plusieurs composants : 
  * manatee : gestion du corpus (binaire en ligne de commande, p.ex compilecorp)
  * bonito : Partie REST API de l'application web, écrite en python. Point d'entrée /bonito/run.cgi
  * gdex : Un plugin de bonito
  * crystal : Le frontend HTML/JS, qui utilise bonito. Point d'entrée de l'application web /crystal
* Comme on veut héberger dans un sous-rep (apps.fti.unige.ch/cheulex) et qu'on a un proxy, le plus simple est que l'application dockerisée soit aussi dans le même sous-rep. Sinon cela rendrait la config proxy pénible.
* Pour accéder aux fonctions de recherches avancées (qui créé un job avec résultat persistant), nous avons du configurer une install NoSketch avec authentification. Voir les config apache dans conf. L'utilisateur est 'public' avec un mot de passe vide. Pour ne pas avoir les résults des jobs de recherche qui s'accumulent (qu'on voit dans l'UI), on redémarre le container chaque nuit, ce qui supprime ces fichiers.

## Notes technique sur le Dockerfile
* On utilise un multi-stage pour éviter une image finale trop grande
* Il y a deux-trois hack, sous forme de patch de fichier (via sed) :
  * On configure crystal pour qu'il détermine lui-même l'url du endpoint bonito (via du JS et windows.location)
  * On patch la config Crystal pour que l'UI soit readonly (on veut pas laisser aux utilisateurs de passage la possibilité de modifier le corpus)
  * On patch la config Crystal pour enlever la pub pour le produit closed-source.

## Le bug sed résolu en oct 2022
Voir [/corpora/registry/cheulex_de](/corpora/registry/cheulex_de) : Cf commit 84ad6c8e

En bref, la compilation du corpus passe par des appel au logiciel [sed](https://www.gnu.org/software/sed/) qui semblait planter de manière incompréhensible avec la commande `'y/ÄÖÜäöü/AOUaou/'` du coup, je tenté à tout hasard de remplacer ces replace en groupe par six replace individuels, et cela à résolu le problème.
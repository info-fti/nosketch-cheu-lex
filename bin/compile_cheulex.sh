#!/bin/bash

rm -rf /corpora/cheulex_de/indexed
rm -rf /corpora/cheulex_fr/indexed
rm -rf /corpora/cheulex_it/indexed

COMP_ARGS="--no-ske"

compilecorp ${COMP_ARGS} /corpora/registry/cheulex_de
compilecorp ${COMP_ARGS} /corpora/registry/cheulex_fr   
compilecorp ${COMP_ARGS} /corpora/registry/cheulex_it

compilecorp ${COMP_ARGS} /corpora/registry/cheulex_de
compilecorp ${COMP_ARGS} /corpora/registry/cheulex_fr   
compilecorp ${COMP_ARGS} /corpora/registry/cheulex_it
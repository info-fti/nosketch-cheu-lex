
ARG MAIN_IMAGE="ubuntu:22.04"

# Multi stage docker file
FROM ${MAIN_IMAGE} as builder


###
## Multi stage part 1 : Building Nosketch
###

# Install nosketch engine build dependencies
RUN apt-get update 
RUN apt-get install -y \
    build-essential \
    checkinstall \
    libpcre3-dev \
    libltdl-dev \
    python3-dev \
    swig \
    libffi-dev \
    python3-setuptools \
    wget


WORKDIR /tmp/

# We keep tarballs in the project, since nosketch have download url which are not fixed
# that is, they keep latest version with '/current/src/' in the URL, so as soon as they
# would publish a new version, our current Dockerfile wouldn't build anymore.
COPY tarballs/* /tmp/

# Exploding tar balls
RUN tar xvfz manatee* && \
    tar xvfz bonito* &&  \
    tar xvfz gdex* && \
    tar xvfz crystal*

##
## Building Manatee
##
# Somehow, we must set prefix as below to make sure
# everything goes into /usr/local prefix ..
# Pretty sure it is a bug and this should be double checked
# in future versions.
RUN cd manatee* && \
    ./configure --prefix=/usr --exec-prefix=/usr/local --with-pcre && \
    make && \
    make install

##
## Building Bonito
##
RUN cd bonito* && \
    ./configure --prefix=/usr && \
    make && \
    make install && \
    ./setupbonito /var/www/bonito /var/lib/bonito && \
    chown -R www-data:www-data /var/lib/bonito && \
# We don't want distributed .htaccess interfere with our apache config.    
    rm -rf /var/www/bonito/.htaccess

##
## Building GDEX
##
ARG gdex_version=4.12
RUN cd gdex* && \
    sed -i "s/<version>/${gdex_version}/g" setup.py && \
    ./setup.py build && \
    ./setup.py install

##
## Building Crystal
##
ARG crystal_version=2.124
### HACK: Modify npm install command in Makefile to handle "permission denied"
RUN sed  -i 's/npm install/npm install --unsafe-perm=true/' crystal*/Makefile
RUN cd crystal-* && \
    make && \
    make install VERSION=${crystal_version}

### HACK: modify URL_BONITO to be set dynamically to the request domain in every request
RUN sed -i 's|URL_BONITO: "http://.*|URL_BONITO: window.location.origin + window.location.pathname + "../bonito/run.cgi/",|' \
        /var/www/crystal/config.js    

### We want crystal UI being readonly
RUN sed -i 's/READ_ONLY: false,/READ_ONLY: true,/' /var/www/crystal/config.js

### We don't want add banner
RUN sed -i 's/HIDE_DASHBOARD_BANNER: false,/HIDE_DASHBOARD_BANNER: true,/' /var/www/crystal/config.js

###
# Multi stage part 2 : We keeps only build material needed to run the application
# (comment 4 lines below to experiment/debug raw image and/or Dockerfile)
###
FROM ${MAIN_IMAGE}
COPY --from=builder /usr/local /usr/local
COPY --from=builder /var/www /var/www
COPY --from=builder /var/lib/bonito /var/lib/bonito

RUN apt-get update && apt-get install -y \
    apache2 \
    python3-prctl \
    python3-openpyxl \
    libpcre3-dev \
    libltdl-dev

# Enable apache CGI and mod_rewrite
RUN a2enmod cgi rewrite

COPY bin/run.cgi /var/www/bonito/run.cgi
COPY bin/compile_cheulex.sh /usr/local/bin/compile_cheulex.sh

# Making sure components compiled above are available 
# on PYTHONPATH some compile from the shell work.
ENV PYTHONPATH="/usr/local/lib/python3.10/dist-packages"

EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
